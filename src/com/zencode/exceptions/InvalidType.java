package com.zencode.exceptions;

public class InvalidType extends Exception {
    public InvalidType(String message) {
        super(message);
    }
}
