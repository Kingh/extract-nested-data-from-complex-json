package com.zencode.services;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.List;
import java.util.Set;

public class ExtractValue {
    public static List<Object> extract(JSONObject jsonObject, String key, List<Object> list) {
        Set keys = jsonObject.keySet();

        if (keys.contains(key)) {
            list.add(jsonObject.get(key));
        } else {
            for (Object k: keys) {
                Object elem = jsonObject.get(k);

                if (elem instanceof JSONArray) {
                    for (Object ob: (JSONArray)elem) {
                        if (ob instanceof JSONObject) {
                            extract((JSONObject) ob, key, list);
                        }
                    }
                } else if (elem instanceof JSONObject) {
                    extract((JSONObject) elem, key, list);
                }
            }
        }

        return list;
    }
}
