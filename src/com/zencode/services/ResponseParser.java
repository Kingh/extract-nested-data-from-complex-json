package com.zencode.services;

import com.zencode.exceptions.InvalidType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

public class ResponseParser {
    /**
     *  Parses a FileReader or String to JSONObject.
     *
     * @param response the Object to be parsed
     * @param <T> param of type FileReader or String.
     * @return JSONObject
     * @throws InvalidType throw InvalidType exception if type is not
     * FileReader or String
     */
    public static <T> JSONObject parser(T response) throws InvalidType {
        JSONParser parser = new JSONParser();
        JSONObject jsonObj = null;

        try {
            if (response instanceof FileReader) {
                jsonObj = (JSONObject) parser.parse((FileReader) response);
            } else if (response instanceof String) {
                jsonObj = (JSONObject) parser.parse((String) response);
            } else {
                throw new InvalidType("Wrong type passed to method");
            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        return jsonObj;
    }
}
