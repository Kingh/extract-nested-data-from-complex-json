package com.zencode.services;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Scanner;

public class Request {
    public static String request(String uri, String method, Map<String, String> params) throws IOException {
        var content = new StringBuilder();
        var url = new URL(uri + uriParams(params));
        var connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod(method);
        connection.connect();
        int responseCode = connection.getResponseCode();
        if (responseCode != 200) {
            throw new RuntimeException("HttpResponseCode: " + responseCode);
        }

        Scanner sc = new Scanner(url.openStream());

        while (sc.hasNext()) {
            content.append(sc.nextLine());
        }

        sc.close();

        return content.toString();
    }

    private static String uriParams(Map<String, String> params) {
        var requestData = new StringBuilder("?");

        for (var param: params.entrySet()) {
            if (requestData.length() != 1) {
                requestData.append('&');
            }

            requestData.append(URLEncoder.encode(param.getKey(), StandardCharsets.UTF_8));
            requestData.append('=');
            requestData.append(URLEncoder.encode(String.valueOf(param.getValue()), StandardCharsets.UTF_8));
        }

        return requestData.toString();
    }
}
