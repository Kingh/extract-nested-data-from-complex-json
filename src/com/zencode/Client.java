package com.zencode;

import com.zencode.exceptions.InvalidType;
import com.zencode.services.ExtractValue;
import com.zencode.services.ResponseParser;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

public class Client {
    public static void main(String[] args) {
        var prop = new Properties();
        var list = new ArrayList<Object>();

        try (InputStream input = new FileInputStream("D:\\Programming\\Java Projects\\ComplexJSON\\config\\api.properties")) {
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }

        final String API_KEY = prop.getProperty("api.key");
        var params = new HashMap<String, String>() {{
            put("origins", "New York City,NY");
            put("destinations", "Washington,DC|Philadelphia,PA|Santa Barbara,CA|Miami,FL|Austin,TX|Napa County,CA");
            put("units", "imperial");
            put("transit_mode", "car");
            put("key", API_KEY);
        }};
        var uri = "https://maps.googleapis.com/maps/api/distancematrix/json";
        var method = "GET";

        try {
            System.out.println(ExtractValue.extract(
                    ResponseParser.parser(
                            new FileReader("D:\\Programming\\Java Projects\\ComplexJSON\\response.json"
                            )
                    ),
                    "text",
                    list
            ));
        } catch (IOException | InvalidType e) {
            e.printStackTrace();
        }
    }
}
